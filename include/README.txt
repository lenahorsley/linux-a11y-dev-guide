The /include directory is used for generic parts which are included to AsciiDoc documents.

Please do not use this directory for any other purposes.
