import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk

def button_clicked(btn):
    dlg = Gtk.AlertDialog(message="You clicked the button.")
    dlg.show()

def on_activate(app):
    win = Gtk.ApplicationWindow(application=app, title='Button with a label')
    button = Gtk.Button(label='Click me!')
    button.connect('clicked', button_clicked)
    win.set_child(button)
    win.present()
    
app = Gtk.Application(application_id='com.example.labeled_button')
app.connect('activate', on_activate)
app.run()