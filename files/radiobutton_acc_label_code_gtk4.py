import sys
import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw


class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_title("GTK4 Radio Button Demo")

        rb_box = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        rb_box.set_margin_top(margin=20)
        rb_box.set_margin_end(margin=20)
        rb_box.set_margin_bottom(margin=20)
        rb_box.set_margin_start(margin=20)
        self.set_child(child=rb_box)

        radio_button1 = Gtk.CheckButton.new_with_label(label="North")
        radio_button1.set_group(group=None)
        radio_button1.connect("toggled", self.on_radio_button_toggled, "North")
        radio_button1.update_property([Gtk.AccessibleProperty.LABEL], ["North radio button"])
        rb_box.append(child=radio_button1)

        radio_button2 = Gtk.CheckButton.new_with_label(label="South")
        radio_button2.set_group(group=radio_button1)
        radio_button2.connect("toggled", self.on_radio_button_toggled, "South")
        radio_button2.update_property([Gtk.AccessibleProperty.LABEL], ["South radio button"])
        rb_box.append(child=radio_button2)

        radio_button3 = Gtk.CheckButton.new_with_label(label="East")
        radio_button3.set_group(group=radio_button1)
        radio_button3.connect("toggled", self.on_radio_button_toggled, "East")
        radio_button3.update_property([Gtk.AccessibleProperty.LABEL], ["East radio button"])
        rb_box.append(child=radio_button3)
        
        radio_button4 = Gtk.CheckButton.new_with_label(label="West")
        radio_button4.set_group(group=radio_button1)
        radio_button4.connect("toggled", self.on_radio_button_toggled, "West")
        radio_button4.update_property([Gtk.AccessibleProperty.LABEL], ["West radio button"])
        rb_box.append(child=radio_button4)


    def on_radio_button_toggled(self, my_radiobutton, my_radiobutton_name):
        if my_radiobutton.get_active():
            state="on"
        else:
            state="off"
        print(my_radiobutton_name, "radio button state:", state)


class MyApp(Adw.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect('activate', self.on_activate)

    def on_activate(self, app):
        self.win = MainWindow(application=app)
        self.win.present()

app = MyApp(application_id="com.example.gtk4_radio_button_accessibility_label_demo")
app.run(sys.argv)