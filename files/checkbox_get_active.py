import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk

def checkbutton_clicked(button):
    dlg = Gtk.AlertDialog(message = f"You toggled the {button.get_label()}, currently active : {button.get_active()}")
    print(button.get_active())
    dlg.show()
    


def on_activate(app: Gtk.Application) -> None:
    
    win = Gtk.ApplicationWindow(application = app)

    checkbutton_1 = Gtk.CheckButton.new_with_label("Check Button 1")

    checkbutton_1.connect('toggled', checkbutton_clicked)
    win.set_child(checkbutton_1)
    win.present()

    


app = Gtk.Application(application_id='org.gtk.Checkbox')
app.connect('activate', on_activate)
app.run()