import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk

ui_string = """                                                                                                                                                                               
<interface>
  <object class="GtkApplicationWindow" id="window">
    <property name="title">Radio Button XML Demo</property>
    <child>
        <object class="GtkBox">
        <child>
            <object class="GtkCheckButton" id="north">
              <property name="label" translatable="yes">North</property>
            </object>
        </child>
        <child>
            <object class="GtkCheckButton" id="south">
              <property name="label" translatable="yes">South</property>
              <property name="group">north</property>
            </object>
        </child>
        <child>
            <object class="GtkCheckButton" id="east">
              <property name="label" translatable="yes">East</property>
              <property name="group">north</property>
            </object>
        </child>
        <child>
            <object class="GtkCheckButton" id="west">
              <property name="label" translatable="yes">West</property>
              <property name="group">north</property>
            </object>
        </child>
        </object>
    </child>
  </object>
</interface>                                                                                                                                                                               
"""

class MyRadioButtonApp(Gtk.Application):
    def __init__(self, ui_string):
        super().__init__(application_id="org.example.GtkRadioButtonXML")
        self.ui_string = ui_string
        self.connect('activate', self.on_activate)

    def on_activate(self, app):
        self.builder = Gtk.Builder.new_from_string(self.ui_string, -1)
        window = self.builder.get_object("window")
        window.set_application (self)
        window.present()
            
    def do_startup(self):
       Gtk.Application.do_startup(self)


my_radio_button_app = MyRadioButtonApp(ui_string)
my_radio_button_app.run()
