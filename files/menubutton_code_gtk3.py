import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import Gio
import sys

class MyWindow(Gtk.ApplicationWindow):
    def __init__(self, app):
        Gtk.Window.__init__(self, title="Menubutton Example", application=app)
        self.set_default_size(350, 200)

        grid = Gtk.Grid()
        
        # Create the MenuButton and attach the items
        menubutton = Gtk.MenuButton(label="Calculator Settings")
        menubutton.set_size_request(85, 35)
        
        grid.attach(menubutton, 0, 0, 1, 1)
        
        menumodel = Gio.Menu()
        menumodel.append("Standard", "app.standard")
        menumodel.append("Scientific", "app.scientific")
        menumodel.append("Programmer", "app.programmer")
        menumodel.append("Statistics", "app.statistics")
        menumodel.append("Quit", "app.quit")

        menubutton.set_menu_model(menumodel)

        self.add(grid)


class MyApplication(Gtk.Application):
    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Set up the actions for each menu item
        standard_action = Gio.SimpleAction.new("standard", None)
        standard_action.connect("activate", self.standard_setting)
        self.add_action(standard_action)

        scientific_action = Gio.SimpleAction.new("scientific", None)
        scientific_action.connect("activate", self.scientific_setting)
        self.add_action(scientific_action)

        programmer_action = Gio.SimpleAction.new("programmer", None)
        programmer_action.connect("activate", self.programmer_setting)
        self.add_action(programmer_action)
        
        statistics_action = Gio.SimpleAction.new("statistics", None)
        statistics_action.connect("activate", self.statistics_setting)
        self.add_action(statistics_action)

        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.quit_cal_setting)
        self.add_action(quit_action)

    
    # Methods associated with menu items   
    def standard_setting(self, action, parameter):
        print("You clicked Standard")

    def scientific_setting(self, action, parameter):
        print("You clicked Scientific")

    def programmer_setting(self, action, parameter):
        print("You clicked Programmer")

    def statistics_setting(self, action, parameter):
        print("You clicked Statistics")

    def quit_cal_setting(self, action, parameter):
        print("You clicked Quit")
        self.quit()


app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)

