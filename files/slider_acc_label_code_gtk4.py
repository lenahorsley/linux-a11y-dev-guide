import sys
import gi
gi.require_version("Gtk","4.0")
gi.require_version("Adw","1")
from gi.repository import Gtk, Adw

class MainWindow(Adw.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_title("GTK4 Scale/Slider Demo")
        self.set_default_size(300, 150)
        self.app_ = self.get_application()
        
        self.mainvbox = Gtk.Box.new(Gtk.Orientation.VERTICAL,5) 
        
        self.set_content(self.mainvbox) 
        
        headerbar = Adw.HeaderBar.new() 
        self.mainvbox.append(headerbar)  
        
        scale = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL,0,4,1)

        scale.set_draw_value(True)
        scale.set_value(2)
        scale.add_mark(0,Gtk.PositionType.BOTTOM,"0")
        scale.add_mark(1,Gtk.PositionType.BOTTOM,"1")
        scale.add_mark(2,Gtk.PositionType.BOTTOM,"2")
        scale.add_mark(3,Gtk.PositionType.BOTTOM,"3")
        scale.add_mark(4,Gtk.PositionType.BOTTOM,"4")
        scale.connect('value-changed', self.slider_changed)
        scale.update_property([Gtk.AccessibleProperty.LABEL], ["scale"])
        self.mainvbox.append(scale)
        
    def slider_changed(self, slider):
        print(int(slider.get_value()))
        

class MyApp(Adw.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def do_activate(self):
        active_window = self.props.active_window
        if active_window:
            active_window.present()
        else:
            self.win = MainWindow(application=self)
            self.win.present()

app = MyApp(application_id="com.example.gtk4_slider_demo")
app.run(sys.argv)

