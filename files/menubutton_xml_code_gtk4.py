import gi
gi.require_version("Gtk", "4.0")
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio


ui_string = """                                                                                                                                                                               
<interface>
  <menu id="my-menu-model">
      <item>
        <attribute name="label">Standard</attribute>
        <attribute name="action">app.standard</attribute>
      </item>
      <item>
        <attribute name="label">Scientific</attribute>
        <attribute name="action">app.scientific</attribute>
      </item>
      <item>
        <attribute name="label">Programmer</attribute>
        <attribute name="action">app.programmer</attribute>
      </item>
      <item>
        <attribute name="label">Statistics</attribute>
        <attribute name="action">app.statistics</attribute>
      </item>
      <item>
        <attribute name="label">Quit</attribute>
        <attribute name="action">app.quit</attribute>
      </item>
  </menu>
  <object class="GtkApplicationWindow" id="window">
    <property name="title">Menu Button Demo</property>
    <child>
      <object class="GtkMenuButton">
        <property name="label">Menu Button Demo</property>
        <property name="menu-model">my-menu-model</property>
      </object>
    </child>
  </object>
</interface>                                                                                                                                                                               
"""

class MyMenuButtonApp(Gtk.Application):
    def __init__(self, ui_string):
        super().__init__(application_id="org.example.Gtk4MenuButton")
        self.ui_string = ui_string
        self.connect('activate', self.on_activate)

    def on_activate(self, app):
        self.builder = Gtk.Builder.new_from_string(self.ui_string, -1)
        window = self.builder.get_object("window")
        window.set_application (self)
        window.present()
        
    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Set up the actions for each menu item
        standard_action = Gio.SimpleAction.new("standard", None)
        standard_action.connect("activate", self.standard_setting)
        self.add_action(standard_action)

        scientific_action = Gio.SimpleAction.new("scientific", None)
        scientific_action.connect("activate", self.scientific_setting)
        self.add_action(scientific_action)

        programmer_action = Gio.SimpleAction.new("programmer", None)
        programmer_action.connect("activate", self.programmer_setting)
        self.add_action(programmer_action)
        
        statistics_action = Gio.SimpleAction.new("statistics", None)
        statistics_action.connect("activate", self.statistics_setting)
        self.add_action(statistics_action)

        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.quit_cal_setting)
        self.add_action(quit_action)

    
    # Methods associated with menu items   
    def standard_setting(self, action, parameter):
        print("Standard")

    def scientific_setting(self, action, parameter):
        print("Scientific")

    def programmer_setting(self, action, parameter):
        print("Programmer")

    def statistics_setting(self, action, parameter):
        print("Statistics")

    def quit_cal_setting(self, action, parameter):
        print("Quit")
        self.quit()


my_menu_button_app = MyMenuButtonApp(ui_string)
my_menu_button_app.run()

