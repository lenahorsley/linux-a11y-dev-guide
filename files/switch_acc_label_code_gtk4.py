import sys
import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk,Gio

class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_title("GTK4 Switch Demo")
        self.app_ = self.get_application()
        self.set_default_size(300, 50)
        
        self.main_vertical_box = Gtk.Box.new( Gtk.Orientation.VERTICAL,20)
        self.set_child(self.main_vertical_box)
        
        hbox = Gtk.Box.new( Gtk.Orientation.HORIZONTAL,20)
        self.main_vertical_box.append(hbox)

        switch = Gtk.Switch.new()
        switch.connect("notify::active",self.on_switch_active_changed) 
        switch.update_property([Gtk.AccessibleProperty.LABEL], ["switch"])
        
        
        self.label = Gtk.Label(label="_Light switch", use_underline=True)
        hbox.append(self.label)
        hbox.append(switch)
        self.label.set_mnemonic_widget(switch)
        hbox.set_spacing(10)
        
    def on_switch_active_changed(self,switch,property_):
        is_active = switch.props.active 
        if is_active:
            print("switch ON")
        else:
            print("switch OFF")

class MyApp(Gtk.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
    def do_activate(self):
        active_window = self.props.active_window
        if active_window:
            active_window.present()
        else:
            self.win = MainWindow(application=self)
            self.win.present()

app = MyApp(application_id="com.example.gtk4_switch_demo")
app.run(sys.argv)