import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk


def on_activate(app: Gtk.Application) -> None:
    
    win = Gtk.ApplicationWindow(application = app)

    checkbutton_1 = Gtk.CheckButton.new_with_label("Check Button 1")

    win.set_child(checkbutton_1)
    win.present()

app = Gtk.Application(application_id='org.gtk.Checkbox')
app.connect('activate', on_activate)
app.run()