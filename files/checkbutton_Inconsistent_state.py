import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk

def on_activate(app: Gtk.Application) -> None:
    win = Gtk.ApplicationWindow(application=app)

    checkbutton_1 = Gtk.CheckButton.new_with_label("Check Button 1")
    checkbutton_2 = Gtk.CheckButton.new_with_label("Check Button 2")

    checkbutton_1.set_tooltip_text("Checkbutton 1 tooltip.")
    checkbutton_2.set_tooltip_text("Checkbutton 2 tooltip.")
    checkbutton_2.set_inconsistent(True)


    grid = Gtk.Grid()
    grid.attach(checkbutton_1, 0, 0, 1, 1)
    grid.attach(checkbutton_2, 0, 1, 1, 1)

    win.set_child(grid)
    win.present()

app = Gtk.Application(application_id='org.gtk.Checkbox')
app.connect('activate', on_activate)
app.run()

