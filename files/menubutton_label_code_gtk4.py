import sys
import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio


class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_title("GTK4 MenuButton Demo")

        # Create the header bar
        self.header = Gtk.HeaderBar()
        self.set_titlebar(self.header)

        # Create actions for each menu item
        standard_action = Gio.SimpleAction.new("standard", None)
        standard_action.connect("activate", self.standard_setting)
        self.add_action(standard_action)  
        
        scientific_action = Gio.SimpleAction.new("scientific", None)
        scientific_action.connect("activate", self.scientific_setting)
        self.add_action(scientific_action)  
        
        programmer_action = Gio.SimpleAction.new("programmer", None)
        programmer_action.connect("activate", self.pprogrammer_setting)
        self.add_action(programmer_action)  
        
        statistics_action = Gio.SimpleAction.new("statistics", None)
        statistics_action.connect("activate", self.statistics_setting)
        self.add_action(statistics_action)  
        
        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.quit_app)
        self.add_action(quit_action)  

        # Create the menu and add the items/options
        menu = Gio.Menu.new()
        menu.append("Standard", "win.standard") 
        menu.append("Scientific", "win.scientific") 
        menu.append("Programmer", "win.programmer") 
        menu.append("Statistics", "win.statistics") 
        menu.append("Quit", "win.quit") 

        # Create a popover menu using the Gio.Menu object from above
        self.popover = Gtk.PopoverMenu()  
        self.popover.set_menu_model(menu)

        # Create a hamburger menu button and add the popover menu
        self.hamburger = Gtk.MenuButton()
        self.hamburger.update_property([Gtk.AccessibleProperty.LABEL], ['Open menu'])
        self.hamburger.set_popover(self.popover)
        self.hamburger.set_icon_name("open-menu-symbolic")
        self.hamburger.set_primary("true")  

        # Add the menu button to the header bar
        self.header.pack_start(self.hamburger)

    def standard_setting(self, action, param):
        print("Standard")
    
    def scientific_setting(self, action, param):
        print("Scientific")

    def pprogrammer_setting(self, action, param):
        print("Programmer")
        
    def statistics_setting(self, action, param):
        print("Statistics")
        
    def quit_app(self, action, param):
        print("Quitting application...")
        self.close()
        
                        
class MyApp(Adw.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect('activate', self.on_activate)

    def on_activate(self, app):
        self.win = MainWindow(application=app)
        self.win.present()

app = MyApp(application_id="com.example.gtk4_menubutton_demo")
app.run(sys.argv)
