import sys
import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio


class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_title("GTK4 Menu Demo")

        # Create the menubar
        main_menu_bar = Gio.Menu.new()
        
        # Create the items "Open," "Save," and "Exit." Attach them to the File menu.
        file_menu = Gio.MenuItem.new('File')
        file_submenu = Gio.Menu.new()
        file_submenu.append_item(Gio.MenuItem.new('Open', 'win.open'))
        file_submenu.append_item(Gio.MenuItem.new('Save', 'win.save'))
        file_submenu.append_item(Gio.MenuItem.new('Exit', 'win.quit'))
        file_menu.set_submenu(file_submenu)
       
        # Create the items "Cut," "Copy," and "Paste." Attach them to Edit menu.
        edit_menu = Gio.MenuItem.new('Edit')
        edit_submenu = Gio.Menu.new()
        edit_submenu.append_item(Gio.MenuItem.new('Cut', 'win.cut'))
        edit_submenu.append_item(Gio.MenuItem.new('Copy', 'win.copy'))
        edit_submenu.append_item( Gio.MenuItem.new('Paste', 'win.paste'))
        edit_menu.set_submenu(edit_submenu)
        
        # Attach the Edit and File menus to the menubar
        main_menu_bar.append_item(file_menu)
        main_menu_bar.append_item(edit_menu)

        app.set_menubar(main_menu_bar)

        # Set up the actions for each menu item
        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.on_close)
        self.add_action(quit_action)  
        
        open_action = Gio.SimpleAction.new('open', None)
        open_action.connect("activate", self.open_file)
        self.add_action(open_action)  
        
        save_action = Gio.SimpleAction.new('save', None)
        save_action.connect("activate", self.save_file)
        self.add_action(save_action)  
        
        copy_action = Gio.SimpleAction.new('copy', None)
        copy_action.connect("activate", self.copy_text)
        self.add_action(copy_action)  
        
        cut_action = Gio.SimpleAction.new('cut', None)
        cut_action.connect("activate", self.cut_text)
        self.add_action(cut_action)  
        
        paste_action = Gio.SimpleAction.new('paste', None)
        paste_action.connect("activate", self.paste_text)
        self.add_action(paste_action) 

        self.set_show_menubar(True)
        self.set_default_size(350, 250)
    
        
    # Methods associated with menu items   
    def on_close(self, action, param):     
        print("Quitting the application")
        self.close()

    def open_file(self, action, param):
        print("Open File")

    def save_file(self, action, param):
        print("Save File")

    def cut_text(self, action, param):
        print("Cut Text")

    def copy_text(self, action, param):
        print("Copy Text")
    
    def paste_text(self, action, param):
        print("Paste Text")


class MyApp(Adw.Application):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect('activate', self.on_activate)

    def on_activate(self, app):
        self.win = MainWindow(application=app)
        self.win.present()

app = MyApp(application_id="com.example.gtk4_menu_demo")
app.run(sys.argv)
