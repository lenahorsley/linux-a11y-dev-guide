:author:  Lena Horsley
:revdate: 2023-10-30
:toc:
:listing-caption: Listing
include::../include/header-assemblies.adoc[]
= Sliders

Sliders allow a user to select a value from a fixed range, where tick (or text) marks designate important values. 

Generally, adhering to the https://developer.gnome.org/hig/patterns/controls/sliders.html[GNOME Human Interface Guidelines concerning sliders] is a good practice. However, from the perspective of a11y, there are some other considerations, which will be discussed below.

== Usage by visually impaired individuals

include::../modules/dev.gtk.sliders.usage.adoc[]


== A properly behaving slider

include::../modules/dev.gtk.sliders.examples.adoc[]