= SW assistive technologies =

In most cases, software helpers for low vision users are not big applications.
They are rather small features of existing applications or some setting possibilities of desktop environments.
We can divide them to several groups:


== Make things bigger ==

The first problem for many users with vision disorders is that default user interface elements are too small.
It is usually very helpful, if such users can adjust the size of text, icons, cursors, etc.

Large text::
  System text size affects menus, window titles, dialog texts and more.
  For some users, it is sufficient to enlarge the system texts (to 140 %, 200 %, ...)
  Most of desktop environments, incl. Gnome and Xfce, offer easy ways to do it.
  Sometimes it has a predefined value, sometimes it can be set a scale factor, in some cases font size can be specified.
  Xfce even allows to set custom DPI for fonts.

Custom font::
  For some users, the font size is not enough.
  They also need to change other https://en.wikipedia.org/wiki/Font#Characteristics[font characteristics] or even whole typeface.
  Sometimes fixed-width letters are better readable than proportional.
  Most of desktop environments, incl. Gnome and Xfce, offer a way to choose custom font.

Icons size::
  Besides text, icons are also popular elements of modern GUI.
  Unlike system font, icon size is not set centrally (at one place), but per case (e.g. for desktop desktops, icons in system panel, icons in individual applications, ...)
  Some desktops (incl. Xfce) and applications (e.g. LibreOffice) allow the user to set icon sizes.

Cursor size::
  Another important control element is the cursor (AKA mouse pointer).
  Its shape and size is controlled by the desktop environment (or window manager).
  In most cases (incl. Gnome, MATE, Xfce) it is adjustable.

Content zoom::
  Not only the control elements, but also (and mainly) the possibility to zoom the content of the application (typically the inner area of the window) is important for many users.
  Fortunately many applications (incl. office suites, text editors, web browsers, graphic editors/viewers, video players, ...) offer zoom features.
  In addition the control methods for content zooming is more or less standardized -- kbd:[Ctrl + mouse wheel] or kbd:[Ctrl + +] / kbd:[Ctrl + -].
  Some applications (e.g. Firefox) offers zooming of either the whole content or texts only.

Screen resolution / Scale / DPI::
  Nowadays (2023) nearly all displays (incl. notebooks) comes with at least full HD (1920 x 1080) resolution.
  An easy way to enlarge all components of GUI is to set lower resolution.
  Most applications are fully usable even with 900 screen lines.
  But some applications have problems already on 768 lines (e.g. LibreOffice Calc - the Sheet menu).
  On 800x600 application windows split to panels (e.g. Brasero, Thunderbird, Geeqie, ...) are hardly usable and some windows (e.g. LibreOffice options) do not fit the screen and become unusable.
  The problem is even worse when "large text" is active.
  Some (X)HDPI screens make GUI elements too small even for common users.
  That is the reason why modern display setting tools (at least in Gnome and Xfce) offers picture scale (fix or custom), which enlarge all widgets to be usable again.

Screen magnifier::
+
--
One of few really complex assistive applications for low vision users is a screen magnifier.
There are several approaches:

- zoom only some area around mouse pointer, while the rest of screen uses standard picture size;
- enlarge one half of screen, while the second half holds standard picture size and scrolls if necessary;
- magnify whole screen, so only a part of whole picture is visible, but it moves along with (mouse) cursor.

Some magnifiers uses only one of these methods, some offer more of them.
Another question is, what should be magnified.
The simplest magnifiers only care about the mouse pointer.
More complex ones also takes text cursor or keyboard focus into account.
The view moves to the place where the most recent event happened.

Better magnifiers offer additional features:

- Crosshairs help to identify the object or exact pixel under focus.
- Color effects allow user to customize brightness and contrast while zoom is on. Some can even invert colors.

Desktop environments, incl. Gnome and Xfce, usually have some built-in magnifier features.
But there are also standalone screen magnifier applications, e.g. *kmag*.
--


== Colors matter ==

For some types of vision disorders certain combinations or interactions of colors are important, either in positive or negative way.
Some users need very light GUI to see even something, while for others even common light intensity is harmful.

For color-blind people, small colorful objects (like icons) could be unrecognizable, while the same in one color is pretty good.
They also need enough dark/light difference between neighboring object to distinguish them, because they perceive colors of similar brightness (e.g. white and yellow) or darkness (e.g. black, brown and red) as equal.
Thus (dark) red text on black background (which is expressive for most users) is almost invisible for the color-blind.

Color themes::
  A system color theme is rather complex set that contains not only colors for text and background.
  Text can be selected or shadowed (inactive), visited/unvisited hypertext links are also distinguished.
  Different background is (or can be) in working area, menus, panels, buttons, title bars, ...

Icon themes::
  Most GUI environments has the ability to change default set of icons.
  Alternative icon set are usually available, offering larger, more contrast, less colorful, ... alternatives.
  Even some applications (e.g. LibreOffice) can change its default icons.

Cursor themes::
  Most GUI environments has the ability to change default set of cursors (i.e. mouse pointers).
  Alternative cursors can be larger, more contrast in some cases even animated.

High contrast::
  It is a preset combination of high contrast colors, icons (and/or cursor).
  It is usually dark text on light background to achieve the highest possible contrast.
  This helps the users with lower sensitivity to light to recognize the text.

Dark mode::
  It is a preset combination of themes for colors, icons (and/or cursor).
  The main idea is light text on dark background to minimize the quantity of light emitted by the screen.
  This helps especially the users with photophobia to avoid being dazzled (blinded) by percepting too much light.

Invert colors / night mode::
  Some applications do not implement system color theme to their content automatically.
  They are document viewers, e-book readers, web browsers, ...
  Nevertheless they usually has some feature to invert colors.
  In some cases, it is called "Night mode".
  Sometimes, such feature is only available via an extension (e.g. Dark reader for Firefox).
  It is a good idea to have a key shortcut for such feature.
  It is also wise not to invert bitmap pictures (especially photos), if possible.


== Sound helps ==

Alert sound::
  Some applications (e.g. terminal emulators) uses a sound to warn user, that something has happened or something is wrong.
  Sometimes it is called a system bell.
  Some desktop environments (e.g. Gnome) allows user to choose the sound used as bell.

Sound notifications::
  Like terminal, also some GUI applications use sound to notify specific events.
  Gnome has predefined sounds for device connect/disconnect, sound volume changes etd.
  Some end-user applications also notify (e.g. incoming message/call) by a sound -- e-mail/chat clients, soft phones, ...

Event sounds::
  Users, who look at the screen from a short distance, may not notice a dialog or notification appearing in another area that they are focused to.
  In such cases, a sound notification is really helpful.
  Some environments whole sets of sounds, each associated to special event (e.g. window opened, menu closed, ...)
  They are called sound themes (KDE) or event sounds (Xfce).

Sound keys::
  Sound notification is sometimes used to announce press of specified keys.
  E.g. Gnome has a _Sound keys_ feature which beeps when Num Lock or Caps Lock are turned on or off.


== Other ==

Window actions::
  When a low resolution and/or large text features are used, some large windows do not fit the screen.
  In such cases, user is unable to reach window bar to move the window, corners to resize the window or action buttons at the bottom.
  Then it is very desirable to reach the window actions via keyboard.
  Many desktops allow user to call the features via key shortcuts: kbd:[Alt + F7] = move, kbd:[Alt + F8] = resize, kbd:[Alt + F10] = (un)maximize.
  Other way is a key shortcut to open the window operations menu: kbd:[Alt + Space].
  And another useful feature is a "window action key" (as called in Gnome), typically kbd:[Alt] or kbd:[Super], which (while pressed) allows to move the window by mouse (while left button pressed).

Locate pointer::
  Some people do not need to enlarge mouse pointer, but from time to time it is hard to find it on screen.
  For them, there is a possibility to activate special effect (some animation) at the cursor position by a key (e.g. kbd:[Ctrl]).

Blinking cursor::
  Flashing text cursor helps to find it on the screen.
  That is why many applications (text editors, terminal emulators, ...) offer this feature.
  Sometimes also various cursor shapes are available.
