=== Toggle buttons
All the preceding labeling examples apply similarly to a toggle button as well. If you can, and you in most cases can, use a `GtkToggleButton`, label it properly, and you're good.

=== Making anything look like a button
In some rare circumstances, it might be necessary to create a custom widget which does not contain any `GtkButton` widgets. In that case, you must ensure that your widget has the accessible role of `GTK_ACCESSIBLE_ROLE_BUTTON`, has an accessibility label, through an accessible label or a labelled by relation, and it respects all the keyboard interactions of a button.

=== Making anything look like a toggle button
Making a custom widget behave as a toggle button is somewhat more innvolved than a regular button. It needs the `GTK_ACCESSIBLE_ROLE_TOGGLE_BUTTON` role, and in addition, it needs to have the correct `GTK_ACCESSIBLE_STATE_PRESSED` every time, e. g. `TRUE` when it's pressed, and `FALSE` when it's not.
