== Tools and components for accessibility development and testing ==

- For blind users:
  ** xref:tools.adoc[How to Install and Configure Tools for the Blind in RHEL and Fedora]

- For low-vision users:
  ** http://colororacle.org/[Color Oracle] -- Free desktop color blindness simulator (needs Java).
  ** http://www.color-blindness.com/coblis-color-blindness-simulator/[Coblis] -- Online Color Blindness Simulator for images.
  ** https://www.toptal.com/designers/colorfilter[Colorblind Web Page Filter] -- Online tool that shows any website with a selected color filter.
  ** https://contrast-grid.eightshapes.com/[Contrast Grid] -- Online grid showing different color combinations for text and background at the same time.
  ** https://colourcontrast.cc/[Colour Contrast Checker] -- Online tool showing example text with custom size and color settings.
