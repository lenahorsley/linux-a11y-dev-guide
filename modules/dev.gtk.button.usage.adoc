As the first principle, the user must know that an user interface element is a button. This is conveyed using the correct element's role.

When the button receives focus, activating it is done using the kbd:[Space] or kbd:[Enter] keys.

Access keys can be, of course, used by visually impaired users, however the discoverability of them is poor, as there is no standard way how they are reported to screen readers.
