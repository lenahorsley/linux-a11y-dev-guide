== Inconsistent State of CheckBox
In general a CheckBox could be in state of On or Off, apart from that it can also be in an state of Neither Off nor On, it is called Inconsistent state. This is used when user selects a range of elements that are affected by a check button, and the current values in that range are inconsistent. 

The following shows an example how to set a CheckBox to Inconsistent state.

[,python]
----
include::../files/checkbutton_Inconsistent_state.py[]
----

image::../images/checkbutton_Inconsistent_state.jpg[]

=== Creating your own

To make a custom widget behave like a check button, it needs the role of `GTK_ACCESSIBLE_ROLE_CHECK_BOX`. In addition, it needs the proper `GTK_ACCESSIBLE_STATE_CHECKED` at all times, so either `TRUE` or `FALSE`, or `GTK_ACCESSIBLE_TRISTATE_MIXED` in the case of the inconsistent state.
